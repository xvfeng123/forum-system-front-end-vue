import Vue from 'vue'
import Router from 'vue-router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import register from "@/components/pages/register";
import login from "@/components/pages/login";
import index from "@/components/pages/index";
import admin from "@/components/adminPages/admin";
import classifyAdmin from "@/components/adminPages/classifyAdmin";
import userListAdmin from "@/components/adminPages/userListAdmin";
import postBarAdmin from "@/components/adminPages/postBarAdmin";
import postAdmin from "@/components/adminPages/postAdmin";
import classifytmp from "@/components/pages/classifytmp";
import indexContent from "@/components/component/indexContent";
import publishPost from "@/components/pages/publishPost";
import wangEditor from "@/components/wangEditor";
import postBarIndex from "@/components/pages/postBarIndex";
import postInfo from "@/components/pages/postInfo";
import errorPage from "@/components/pages/errorPage";
import notFoundPage from "@/components/pages/notFoundPage";
import searchResultPage from "@/components/pages/searchResultPage";
import myIndex from "@/components/pages/myIndex";
import test from "@/components/config/test";
import setTopPostsAdmin from "@/components/adminPages/setTopPostsAdmin";
import categoryPage from "@/components/pages/categoryPage";
import carouselSettingAdmin from "@/components/adminPages/carouselSettingAdmin";
import carouselEditAdmin from "@/components/adminPages/carouselEditAdmin";
import myMsg from "@/components/pages/msgPages/myMsg";
import reply from "@/components/pages/msgPages/reply";
import collect from "@/components/pages/msgPages/collect";
import comment from "@/components/pages/msgPages/comment";
import subscribe from "@/components/pages/msgPages/subscribe";

Vue.use(Router);
Vue.use(ElementUI);

export default new Router({
    // 如果不想要很丑的 hash，我们可以用路由的 history 模式，这种模式充分利用 history.pushState API 来完成 URL 跳转而无须重新加载页面。
    mode: 'history', // 去除地址栏中的 #  (改为 history 模式  )
    routes:[
        {
            path: '/test',
            name: 'test',
            component: test
        },
        {
            path: '/login',
            name: 'login',
            component: login
        },
        {
            path:'/reg',
            name: 'register',
            component: register
        },
        {
            path: '/',
            name: 'index',
            component: index
        },
        {
            path: '/classifytmp',
            name: 'classifytmp',
            component: classifytmp
        },
        {
            /*就算你直接访问 子路由， 也会先经过 父路由 /admin*/
            path: '/admin',
            name: 'admin',
            component: admin,
            children: [  /*引入子路由*/
                {
                    path: '/admin/classifyAdmin',
                    name: 'classifyAdmin',
                    component: classifyAdmin
                },
                {
                    path: '/admin/userListAdmin',
                    name: 'userListAdmin',
                    component: userListAdmin
                },
                {
                    path: '/admin/postBarAdmin',
                    name: 'postBarAdmin',
                    component: postBarAdmin
                },
                {
                    path: '/admin/postAdmin',
                    name: 'postAdmin',
                    component: postAdmin
                },
                {
                    path: '/admin/setTopAdmin',
                    name: 'setTopPostsAdmin',
                    component: setTopPostsAdmin
                },
                {
                    path: '/admin/carouselSetting',
                    name: 'carouselSettingAdmin',
                    component: carouselSettingAdmin
                },
                {
                    path: '/admin/carouselEdit',
                    name: 'carouselEditAdmin',
                    component: carouselEditAdmin
                },
            ]
        },
        {
            path: '/indexContent',
            name: 'indexContent',
            component: indexContent
        },
        {
            path: '/publishPost',
            name: 'publishPost',
            component: publishPost
        },
        {
            path: '/wang',
            name: 'wangEditor',
            component: wangEditor
        },
        {
            path: '/postBarIndex',
            name: 'postBarIndex',
            component: postBarIndex
        },
        {
            path: '/postInfo',
            name: 'postInfo',
            component: postInfo
        },
        {
            path: '/errorPage',
            name: 'errorPage',
            component: errorPage
        },
        {
            path: '/searchResult',
            name: 'searchResultPage',
            component: searchResultPage
        },
        {
            path: '/myIndex',
            name: 'myIndex',
            component: myIndex
        },
        {
            path: '/ctg',
            name: 'categoryPage',
            component: categoryPage
        },
        {
            path: '/myMsg',
            name: 'myMsg',
            component: myMsg,
            children: [  /*引入子路由*/
                {
                    // path: '/reply',
                    path: '/myMsg/reply',  /*子路由不一定要 以父路由开头 上面的 /reply 也是可以的*/
                    name: 'reply',
                    component: reply
                },
                {
                    path: '/myMsg/comment',  /*子路由不一定要 以父路由开头 上面的 /reply 也是可以的*/
                    name: 'comment',
                    component: comment
                },
                {
                    path: '/myMsg/subscribe',  /*子路由不一定要 以父路由开头 上面的 /reply 也是可以的*/
                    name: 'subscribe',
                    component: subscribe
                },
                {
                    path: '/myMsg/collect',  /*子路由不一定要 以父路由开头 上面的 /reply 也是可以的*/
                    name: 'collect',
                    component: collect
                },
            ]
        },
        // 当访问一个不存在的页面路径时， 它默认的显示效果是空， 我们这里写一个路径来匹配捕捉这些设计之外的路径，把他引导到一个错误提示界面
        {
            path: '*',
            component: notFoundPage //我这里是引导到 notFoundPage
        },
    ]
})
