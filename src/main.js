import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import "./assets/css/globalcss.css"
import global from "@/components/config/global";

axios.defaults.withCredentials=true;

Vue.config.productionTip = false;
Vue.prototype.$http = axios;
Vue.prototype.GLOBAL = global;

new Vue({
  router: router,
  render: h => h(App),
}).$mount('#app');
